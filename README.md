[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/apptainer/apptainer-singularity-projects/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/apptainer/apptainer-singularity-projects/-/commits/main) 
[![build packages status](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdiamond%2Fapptainer%2Fapptainer-singularity-projects%2Fraw%2Fmain%2Fstatus.json&query=%24.build_coverage&label=build%20coverage&color=green)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/apptainer/apptainer-singularity-projects/-/commits/main)
[![container available](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdiamond%2Fapptainer%2Fapptainer-singularity-projects%2Fraw%2Fmain%2Fstatus.json&query=%24.container_available&label=containers%20available&color=blue)](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/apptainer/apptainer-singularity-projects/-/tree/master/manifests?ref_type=heads) 

# Images Apptainer pour DIAMOND du PEPR DIADEM

Le but de ce dépôt est de fournir les images Apptainer de codes de la communauté des matériaux pour les besoins de DIAMOND. Les fichiers de définitions des différentes images sont disponibles dans le dossier `def-files/`.

## Comment télécharger une image Apptainer

Tout d'abord, assurez-vous d'avoir installé Apptainer sur votre poste de travail. Ensuite, après avoir choisi l'`<image>` qui vous convient [sur ce lien](https://gricad-gitlab.univ-grenoble-alpes.fr/diamond/apptainer/apptainer-singularity-projects/container_registry), veuillez utiliser les commandes suivantes :

```bash
apptainer pull <image>.sif \
    oras://gricad-registry.univ-grenoble-alpes.fr/diamond/apptainer/apptainer-singularity-projects/<image>.sif:latest
``` 

## Comment utiliser l'image Apptainer

Pour des tutoriels spécifiques aux images, merci de vous rendre sur le site du projet Diamond, section [documentation](https://diamond-diadem.github.io/documentation/start-here/home/).
