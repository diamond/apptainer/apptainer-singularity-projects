# For gitlab-ci : regex=cp2k-coreutils-bash-minimal
Bootstrap: localimage
From: /gnu/store/5853vzf1r0lqnry52j8lzzsn8gr7f1nl-cp2k-coreutils-bash-minimal-squashfs-pack.gz.squashfs

%files
    empty_file /etc/passwd # to remove warning about /etc/passwd
    empty_file /etc/group  # to remove warning about /etc/group

%help
    This container embedds CP2K (version v2024.1) with full CPU support.
    For more information about this image, please run "apptainer inspect <this-image>"

    If you want to run any executable from the CP2K suite, you can use
    "apptainer exec <this-image> <executable-name>".

    You can also enter an interactive shell within the container with "apptainer shell <this-image>".
    However, please note almost no basic Linux command are available within the container 
    (both for portability and security reasons).

%labels
    Owner The CP2K Group.
    Author benjamin.arrondeau@univ-grenoble-alpes.fr
    Version v2024.1
    Label cp2k-v2024.1
    EntryPoint https://www.cp2k.org/
