# For gitlab-ci : regex=openmpi-bash-minimal-gcc-toolchain
Bootstrap: localimage
From: /gnu/store/i9g1b5zdwkaz8driclp5frqsgm37zrq2-openmpi-bash-minimal-gcc-toolchain-bc-squashfs-pack.gz.squashfs

%files
    input-files/tutorial-sources/my-matmul-mpi.c /opt/code-tutorial/my-matmul-mpi.c
    input-files/tutorial-sources/profiler-cpu-matmul.sh /opt/bin-tutorial/profiler-cpu-matmul.sh 
    empty_file /etc/passwd # to remove warning about /etc/passwd
    empty_file /etc/group  # to remove warning about /etc/group

%post
    # Generating the binary
    cd /opt/code-tutorial
    mpicc my-matmul-mpi.c -O2 -o /bin/parallel-benchmark

%test
    cd $HOME
    echo "% Running final stage tests %"
    mpirun -np 4 --allow-run-as-root parallel-benchmark 100

%runscript
    parallel-benchmark $*

%help
    This container image embedds a tutorial to exhibit OpenMPI characteristic behaviour when
    used with Apptainer. It intends to exhibit the difference between embedded OpenMPI and
    hybrid host/container OpenMPI execution.

    If you use "apptainer run <this-image> [arguments]", the main executable of this image
    will be called : "parallel-benchmark", which is a small O(N^3) program. By default, 
    N=100 but you are free to give another value as "[argument]".

    You can also specify the command you want to run with "apptainer exec <this-image> command".
    For instance, using "apptainer exec <this-image> parallel-benchmark 100" would be strictly
    equivalent to using "apptainer run <this-image> 100".

    You can also enter an interactive shell within the container with
        "apptainer shell <this-image>".
    
    It is then possible to run the "parallel-benchmark" program in parallel using OpenMPI, like
      "apptainer exec <this-image> mpirun -np <N> parallel-benchmark [arguments]".
    In this case, you would be using the OpenMPI library present in the container (embedded
    MPI). This solution is practical in many cases as it does not require any external
    installation on your host machine : all the software you are using are present in the
    container. However, this may prove limiting in a high-performance computing environment
    as the OpenMPI version present in the container is generic, and not optimized for your
    specific hardware. The performance are hence slightly lower, with a maximal CPU use
    typically around 80-90%.

    To overcome this issue, you can run in the so-called hybrid parallelization mode, like
      "mpirun -np <N> apptainer run <this-image> [arguments]", or with "exec"
      "mpirun -np <N> apptainer exec <this-image> parallel-benchmark [arguments]".
    In such cases, it is howewer required that you have OpenMPI installed on the host machine.
    Furthermore, as this hybrid mode has OpenMPI from the container and OpenMPI from the host
    communicating with each other, you may encounter compatibility issues between the two
    versions. If you want to know the version of OpenMPI installed, either on the host machine
    or in the container, you can use the "ompi_info" command.

%labels
    Owner Free to use
    Author dylan.bissuel@univ-lyon1.fr
    Version 1.0
    Label tutorial-openmpi-apptainer-v1.0
