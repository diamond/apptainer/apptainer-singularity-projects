folder="def-files"
## for definition files which do not require time-machine
export matrix_fapptainer=""
## for definition files which do require time-machine
export matrix_apptainer_from_guix=""
for file in "$folder"/*; do
   # This weird line is necessary to avoid exit status 1 because grep does not found any match
   it=$(cat $file | { grep "Bootstrap: localimage"; true; } | wc -l)
   file="${file%.def}"; file="${file#def-files/}"
   if [[ "$it" -gt 0 ]]; then
      matrix_apptainer_from_guix="${matrix_apptainer_from_guix}${file}, "
   else
      matrix_fapptainer="${matrix_fapptainer}${file}, "
   fi
done
matrix_fapptainer="${matrix_fapptainer::-2}"
matrix_apptainer_from_guix="${matrix_apptainer_from_guix::-2}"
