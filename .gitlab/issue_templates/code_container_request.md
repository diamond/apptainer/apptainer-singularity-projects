## Proposal

<!-- Use this section to explain which code or container you would like available on the platform. 
    Try to give as much detail as possible on the code / container, 
    i.e. is it free of use ? which dependencies it uses ? which features do you want ? 
    Please use an ordered list. -->

<!-- Please add a label for the type of feature as per https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification -->
/label ~"type::code_container"
