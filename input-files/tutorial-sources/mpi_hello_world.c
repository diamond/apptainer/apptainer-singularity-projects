#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>


// This tutorial code willingly tries to approximate pi in an inefficient manner
// in order to allow for MPI benchmark comparison.

int main(int argc, char** argv) {

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Launching the profiler in the background
    if(world_rank == 0){
        system("bash /opt/bin-tutorial/profiler-cpu.sh &");
    }

    // To compute the CPU runtime
    double start, end;
    if(world_rank == 0){
        start = MPI_Wtime();
    }

    // Reading the first argument (number of terms of the approximating sum) 
    // as a float first to allow for [0-9]e[0-9] like notations.
    double N1;
    long N;
    if(argc == 1) {
        if(world_rank == 0){
            printf("You did not specify the number of iterations.\n");
            printf("Setting it to one million (1e6) iterations (default).\n");
        }
        N = (long) 1e6;
    }
    else if(sscanf(argv[1], "%lf", &N1) != 1) {
        if(world_rank == 0){
            printf("Error, the number of iterations %s can't be understood.\n", argv[1]);
            printf("Setting it to one million (1e6) iterations (default).\n");
        }
        N = (long) 1e6;
    }
    else {
        sscanf(argv[1], "%lf", &N1);
        N = (long) N1; // Cast to int
    }
    
    // Individual (per-process) pi/2, global pi/2, global pi, relative difference and iterator
    double ipi2, gpi2 ,gpi, diff;
    long i;

    // Computing per-process pi/2 part (evenly sharing load between processes)
    ipi2=1.;
    for(i=world_rank; i<=N; i=i+world_size){
        if(i>0){
            ipi2 *= (4. * pow((double) 1.*i, 2)) / 
                    (4. * pow((double) 1.*i, 2) - 1.);  // converges to pi / 2
        }
    }

    // Waiting for all to end, multiplying each individual value and estimating the relative difference
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Reduce(&ipi2, &gpi2, 1, MPI_DOUBLE, MPI_PROD, 0, MPI_COMM_WORLD);
    gpi = 2. * gpi2;
    diff = 100. - fabs(gpi - M_PI) / M_PI * 100.;

    // Finalize the MPI environment.
    MPI_Finalize();

    // Summary of the calculation.
    // Does not include built-in CPU usage estimation.
    if(world_rank == 0){
        end = MPI_Wtime();
        printf("*****************************\n");
        printf("  Approximated pi using %ld terms", N);
        printf("  (with %i MPI cores)\n", world_size);
        printf("  Computed value : %0.17lf (%0.17lf%% accurate)\n", gpi, diff);
        printf("  Runtime : %lf s\n", end-start);
    }

}
