#!/bin/bash

started=false       # Has the mpi application started ?
sumCPU=0
iter=0
while true; do
    if pidof -q parallel-benchmark; then # Detects a process with the correct name !
        if ! $started; then # First time ? it just started !
            started=true
        fi
    else # No process detected
        if $started; then # The calculation already started -> it ended -> exit while loop.
            break
        fi
    fi

    if $started; then # The calculation is running, let's take an instant CPU-usage picture
        for i in $(pidof parallel-benchmark); do
            tcpu=$(ps -p $i -o %cpu | tail -n 1)
            if [[ $tcpu == *"%CPU"* ]]; then # the computation finished between verification and CPU-use measure
                tcpu=0
            else
                iter=$(echo "scale=2 ; $iter + 1.0" | bc)
                sumCPU=$(echo "scale=2 ; $sumCPU + $tcpu" | bc)
            fi
        done
        # Average up to now for CPU usage over the extracted sample
        avg=$(echo "scale=2 ; $sumCPU / $iter" | bc)
        echo "Estimated CPU usage : ${avg}%" > CPU-usage
    fi
    
    sleep 0.001 # Waiting before retrying to see CPU usage

done