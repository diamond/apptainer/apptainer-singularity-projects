#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void printMatrix(int N, double* matrix);
double randfrom(double min, double max);

int main(int argc, char** argv) {

    /* -------------------------------- Initialization ----------------------------------*/

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Launching the profiler in the background
    if(world_rank == 0){
        system("bash /opt/bin-tutorial/profiler-cpu-matmul.sh  &");
        // system("bash ./profiler-cpu-matmul.sh  &");
    }

    // To compute the CPU runtime
    double start, end;
    if(world_rank == 0){
        start = MPI_Wtime();
    }

    // Reading the first argument (size of the square matrixes) 
    // as a float first to allow for [0-9]e[0-9] like notations.
    double N1, T1;
    int N, T;
    if(argc == 1) {
        if(world_rank == 0){
            printf("You did not specify the square matrix size.\n");
            printf("Setting it to 100 (default).\n");
        }
        N = 1e2;
    }
    else if(sscanf(argv[1], "%lf", &N1) != 1) {
        if(world_rank == 0){
            printf("Error, the square matrix size %s can't be understood.\n", argv[1]);
            printf("Setting it to 100 (default).\n");
        }
        N = 1e2;
    }
    else {
        sscanf(argv[1], "%lf", &N1);
        N = (int) N1; // Cast to int
    }
    // Reading the second argument (number of damping steps) 
    // as a float first to allow for [0-9]e[0-9] like notations.
    if(argc < 3) {
        if(world_rank == 0){
            printf("You did not specify the number of matrix multiplications to perform.\n");
            printf("Setting it to 1000 (default).\n");
        }
        T = 1e3;
    }
    else if(sscanf(argv[2], "%lf", &T1) != 1) {
        if(world_rank == 0){
            printf("Error, number of matrix multiplications to perform %s can't be understood.\n", argv[1]);
            printf("Setting it to 1000 (default).\n");
        }
        T = 1e3;
    }
    else {
        sscanf(argv[2], "%lf", &T1);
        T = (int) T1; // Cast to int
    }

    /* --------------------------------Matrix creation ----------------------------------*/    
    int i,j,k,t;
    double* matrix1 = (double*)malloc(N * N * sizeof(double));
    double* matrix2 = (double*)malloc(N * N * sizeof(double));
    double* productMatrix = (double*)malloc(N * N * sizeof(double));
    double* Imatrix1 = (double*)malloc(N * N * sizeof(double));
    double* Imatrix2 = (double*)malloc(N * N * sizeof(double));
    double* IproductMatrix = (double*)malloc(N * N * sizeof(double));
    MPI_Barrier(MPI_COMM_WORLD);
    if(world_rank == 0){
        // Populate the matrices with values
        for (i = 0; i < N; i++)
        {
            for (j = 0; j < N; j++)
            {
                matrix1[i*N + j] = 0.;
                matrix2[i*N + j] = 0.;
                productMatrix[i*N + j] = randfrom(-1.,1.); // initial value
                Imatrix1[i*N + j] = 0.;
                Imatrix2[i*N + j] = 0.;
                IproductMatrix[i*N + j] = 0.;
            }
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(matrix1, N*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(matrix2, N*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(productMatrix, N*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(Imatrix1, N*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(Imatrix2, N*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(IproductMatrix, N*N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    if(world_rank==0){
        if (N<=10){
            // printf("Zero matrix\n");
            // printMatrix(N, matrix1);
            // printf("Zero matrix\n");
            // printMatrix(N, matrix2);
            printf("Initial matrix\n");
            printMatrix(N, productMatrix);
        }
    }

    /* ----------------------- Randomly damping the obtained matrix ---------------------*/
    for (t=0; t<T; t++)
    {
        MPI_Barrier(MPI_COMM_WORLD);
        for(i=world_rank; i<N; i+=world_size)
        {
            for(j=0;j<N;j++)
            {
                Imatrix1[i*N + j] = productMatrix[i*N + j];
                Imatrix2[i*N + j] = randfrom(0., (double) 2./N );
                IproductMatrix[i*N + j] = 0;
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Allreduce(Imatrix1, matrix1, N*N, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);                // m1(t) = pm(t-1)
        MPI_Allreduce(Imatrix2, matrix2, N*N, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);                // m2(t) = random
        MPI_Allreduce(IproductMatrix, productMatrix, N*N, MPI_DOUBLE, MPI_PROD, MPI_COMM_WORLD);   // pm(t) = 04
        MPI_Barrier(MPI_COMM_WORLD);
        if(world_rank==0){
            if (N<=10 && T < 5){
                printf("****** STEP %d **********\n",t);
                printf("Matrix to be multiplied\n");
                printMatrix(N, matrix1);
                printf("Random 0-1 matrix\n");
                printMatrix(N, matrix2);
            }
        }
        for (i=world_rank; i<N; i+=world_size)
        {
            for (j=0; j<N; j++){
                for (k=0; k<N; k++){
                    IproductMatrix[i*N + j] += matrix1[i*N + k] * matrix2[k*N + j] ;
                    Imatrix1[i*N + j] = 0;
                    Imatrix2[i*N + j] = 0;
                }
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Allreduce(Imatrix1, matrix1, N*N, MPI_DOUBLE, MPI_PROD, MPI_COMM_WORLD);               // m1(t) = 0
        MPI_Allreduce(Imatrix2, matrix2, N*N, MPI_DOUBLE, MPI_PROD, MPI_COMM_WORLD);               // m2(t) = 0
        MPI_Allreduce(IproductMatrix, productMatrix, N*N, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);    // pm(t) = pm(t-1) * rand(0,1)
        MPI_Barrier(MPI_COMM_WORLD);
        if(world_rank==0){
            if (N<=10 && T < 5){
                printf("Final matrix\n");
                printMatrix(N, productMatrix);
            }
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }

    /* -------------------------------- Summary of the calculation ----------------------*/
    // Does not include built-in CPU usage estimation.
    if(world_rank == 0){
        end = MPI_Wtime();
        printf("*****************************\n");
        printf("  Performed %d multiplications with %d x %d matrixes.\n", T, N, N);
        printf("  (with %i MPI cores)\n", world_size);
        // printf("  Computed value : %0.17lf (%0.17lf%% accurate)\n", gpi, diff);
        printf("  Runtime : %lf s\n", end-start);
        if (N<=10){
            printf("Product:\n");
            printMatrix(N, productMatrix);
        }
    }

    free(matrix1);
    free(matrix2);
    free(productMatrix);
    free(Imatrix1);
    free(Imatrix2);
    free(IproductMatrix);

    // Finalize the MPI environment.
    MPI_Finalize();

}

void printMatrix(int N, double *matrix)
{
    int i,j;
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            printf("%lf\t", matrix[i*N + j]);
        }
        printf("\n");
    }
    printf("\n");
}

double randfrom(double min, double max) 
{
    double range = (max - min); 
    double div = RAND_MAX / range;
    return min + (rand() / div);
}